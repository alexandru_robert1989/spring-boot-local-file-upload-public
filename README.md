# Simple Spring Boot local file upload with Swagger example

This project represents a Spring Boot project allowing a user to upload files locally, inside a directory
within the projected.

On startup, the app creates the _uploads_directory_ at the project's root. Uploaded files will be stored
within this directory.

As a bonus, the project uses Swagger, a framework that describes API requests and provides a 
user interface allowing the user to test the API's exposed endpoints.

To open Swagger and test file upload, after having launched the project go to : 

http://localhost:8081/swagger-ui.html

The maximum allowed file size is 50 MB. To modify this limit, modify the .properties file.
Please note that Spring Boot does not allow requests larger than 5 GB


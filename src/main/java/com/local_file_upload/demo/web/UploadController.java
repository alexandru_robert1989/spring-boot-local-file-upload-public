package com.local_file_upload.demo.web;

import com.local_file_upload.demo.constant.response.FileUploadDefaultResponseMessage;
import com.local_file_upload.demo.constant.response.ResponseStringMessage;
import com.local_file_upload.demo.exception.*;
import com.local_file_upload.demo.service.FilesStorageService;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.util.List;

@RestController()
@RequestMapping("/test")
@AllArgsConstructor
@Log4j2
public class UploadController {

    private final FilesStorageService filesStorageService;


    @GetMapping("/")
    public ResponseEntity<?> getHomePage() {
        return ResponseEntity.ok("This should be the home page");
    }



    @PostMapping("/save-one-file")
    public ResponseEntity<?> saveFile(@RequestBody MultipartFile file) {

        try {

            filesStorageService.saveFileLocally(file);
            return ResponseEntity.ok(new ResponseStringMessage(FileUploadDefaultResponseMessage.FILE_SAVED_SUCCESSFULLY.getMessage()));
        } catch (FileAlreadyExistsException e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(FileUploadDefaultResponseMessage.FILE_ALREADY_EXISTS.getMessage()));
        } catch (DifferentFileWithSameNameException e) {
            log.info(e.getMessage());
            return ResponseEntity.ok(new ResponseStringMessage(FileUploadDefaultResponseMessage.FILE_SAVED_SUCCESSFULLY.getMessage()));
        } catch (MaxUploadSizeExceededException  e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(FileUploadDefaultResponseMessage.MAX_UPLOAD_SIZE_EXCEEDED.getMessage()));
        } catch (IOException e) {
            log.info(e.getMessage());
            return ResponseEntity.badRequest().body(new ResponseStringMessage(FileUploadDefaultResponseMessage.SAVING_FILE_FAILED.getMessage()));
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(new ResponseStringMessage(FileUploadDefaultResponseMessage.FILE_UPLOAD_UNEXPECTED_EXCEPTION.getMessage()));

        }
    }






}

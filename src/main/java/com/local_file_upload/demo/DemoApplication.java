package com.local_file_upload.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication

public class DemoApplication {

    public static void main(String[] args) {

        System.out.println(System.getProperty("user.dir"));
        SpringApplication.run(DemoApplication.class, args);
    }

}

package com.local_file_upload.demo.service;

import com.local_file_upload.demo.exception.DifferentFileWithSameNameException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;

/**
 * Interface providing the abstract methods that once implemented are used
 * for uploading and downloading files.
 */
public interface FilesStorageService {

    void saveFileLocally(MultipartFile file) throws
            FileAlreadyExistsException, DifferentFileWithSameNameException, IOException;


    void deleteOne(String fileName) throws IOException;

    boolean deleteAll() throws IOException;




}

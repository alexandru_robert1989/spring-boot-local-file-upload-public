package com.local_file_upload.demo.service.impl;

import com.local_file_upload.demo.constant.DefaultPaths;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;


@Service
public class DefaultDirectoriesInitializer {


    @PostConstruct
    void initiateDefaultDirectories() {
        initRootStorageDirectory();
    }

    void initRootStorageDirectory() {
        try {
            if (!Files.exists(DefaultPaths.rootPath)) {
                Files.createDirectory(DefaultPaths.rootPath);
                System.out.println("\t\tStorage folder created at : " + DefaultPaths.rootPath.toAbsolutePath());
            } else {
                //     log.info("Storage folder already exists at : " + root.toAbsolutePath());
                System.out.println("\t\tStorage folder already exists at : " + DefaultPaths.rootPath.toAbsolutePath());
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed creating default upload folder");
        }
    }



}

package com.local_file_upload.demo.service.impl;


import com.local_file_upload.demo.constant.DefaultPaths;
import com.local_file_upload.demo.exception.DifferentFileWithSameNameException;
import com.local_file_upload.demo.service.FilesStorageService;
import com.local_file_upload.demo.utility.FileUtil;
import com.local_file_upload.demo.utility.PathUtil;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Service providing the methods used for uploading, downloading and deleting files.
 */

@Service
public class FilesStorageImpl implements FilesStorageService {


   // String separator = System.getProperty("file.separator");

    private final Path path = DefaultPaths.rootPath;

    private final PathUtil pathUtil;

    private final FileUtil fileUtil;

    private final DefaultDirectoriesInitializer defaultDirectoriesInitializer;

    public FilesStorageImpl(PathUtil pathUtil, FileUtil fileUtil, DefaultDirectoriesInitializer defaultDirectoriesInitializer) {
        this.pathUtil = pathUtil;
        this.fileUtil = fileUtil;
        this.defaultDirectoriesInitializer = defaultDirectoriesInitializer;
    }

    /**
     * Save one file locally.
     *
     * @param file a {@link MultipartFile}
     */
    @Override
    public void saveFileLocally(MultipartFile file) throws
            FileAlreadyExistsException, DifferentFileWithSameNameException, IOException {

        if (fileUtil.fileNameExists(path, file.getOriginalFilename())) {
            System.out.println("File name existed ????");
            fileUtil.handleSameNameExists(path, file);
            System.out.println("Handled file name exists");
        } else {
            System.out.println("File name did not exist");
            try {
                Files.copy(file.getInputStream(), pathUtil.buildFilePathFromFileName(path, file.getOriginalFilename()));
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("Failed saving new file");
            }
        }
    }



    @Override
    public void deleteOne(String fileName) throws IOException {
        Files.deleteIfExists(pathUtil.buildFilePathFromFileName(path, fileName));
    }


    @Override
    public boolean deleteAll() throws IOException {
        FileSystemUtils.deleteRecursively(path.toFile());
        defaultDirectoriesInitializer.initiateDefaultDirectories();
        return true;
    }





}

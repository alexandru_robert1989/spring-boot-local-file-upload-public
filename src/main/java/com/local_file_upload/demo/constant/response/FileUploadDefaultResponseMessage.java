package com.local_file_upload.demo.constant.response;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum FileUploadDefaultResponseMessage {

    FILE_SAVED_SUCCESSFULLY("file_saved_successfully"),
    FILE_ALREADY_EXISTS("file_already_exists"),
    DIFFERENT_FILE_WITH_SAME_NAME_EXISTS("different_file_with_same_name_exists"),
    SAVING_FILE_FAILED("saving_file_failed"),
    MAX_UPLOAD_SIZE_EXCEEDED("max_upload_size_exceeded"),
    DELETING_FILE_FAILEd("deleting_file_failed"),
    DELETING_ALL_FILES_FAILED("deleting_all_files_failed"),
    FILE_UPLOAD_UNEXPECTED_EXCEPTION("file_upload_unexpected_exception");

    private final String message;


}

package com.local_file_upload.demo.constant.response;

/**
 * Class that can be used to return a message as a response to a HTTP request's result.
 * <p>
 * The getters, setters, to String, equalsAndHashCode, noArgConstructor and
 * AllArgsConstructor methods where implemented implicitly using {@link lombok.Lombok} annotations.
 */

import lombok.*;

@Getter
@AllArgsConstructor
public class ResponseStringMessage {

    private String message;
}

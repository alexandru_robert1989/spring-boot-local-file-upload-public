package com.local_file_upload.demo.constant;

import lombok.Getter;
import org.springframework.stereotype.Component;

import java.nio.file.Path;
import java.nio.file.Paths;

@Component
@Getter
public class DefaultPaths {

    private static final String rootStringPath = "uploads_directory";

    public static final Path rootPath = Paths.get(rootStringPath);


}

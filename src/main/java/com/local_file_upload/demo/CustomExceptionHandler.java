package com.local_file_upload.demo;

import com.local_file_upload.demo.constant.response.FileUploadDefaultResponseMessage;
import com.local_file_upload.demo.constant.response.ResponseStringMessage;
import org.apache.tomcat.util.http.fileupload.impl.SizeLimitExceededException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { SizeLimitExceededException.class})
    protected ResponseEntity<?> handleSizeLimitExceededException(RuntimeException ex, WebRequest request) {
        String bodyOfResponse = "This should be application specific";
        System.out.println(ex.getMessage());
        System.out.println(request);
        return ResponseEntity.badRequest().body(new ResponseStringMessage(FileUploadDefaultResponseMessage.MAX_UPLOAD_SIZE_EXCEEDED.getMessage()));
       // return handleExceptionInternal(ex, bodyOfResponse, new HttpHeaders(), HttpStatus.CONFLICT, request);
    }
}

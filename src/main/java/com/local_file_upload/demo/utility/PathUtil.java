package com.local_file_upload.demo.utility;

import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;

public interface PathUtil {

    Path buildFilePathFromFileName(Path root, String fileName);

    Path buildFilePathFromFile(Path root, MultipartFile file);

    String returnFileStringPathFromFileName(Path root, String fileName);

    String returnFileStringPathFromFile(Path root, MultipartFile file);
}

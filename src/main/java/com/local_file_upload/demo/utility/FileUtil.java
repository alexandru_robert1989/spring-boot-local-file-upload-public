package com.local_file_upload.demo.utility;

import com.local_file_upload.demo.exception.DifferentFileWithSameNameException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Path;

public interface FileUtil {

    boolean fileNameExists(Path path, String fileName);

    File getFileByName(Path path, String fileName);

    String getFileExtension(MultipartFile file);

    String renameFileBeforeRecording(Path root, String fileName);

    boolean isSameFile(File fileOne, File fileTwo) throws IOException;

    File storeTemporaryFile(Path path, MultipartFile file, String temporaryFileName) throws IOException;

    void deleteTemporaryFile (Path path, String fileName)throws IOException;

    void handleSameNameExists (Path path, MultipartFile file) throws
            FileAlreadyExistsException, DifferentFileWithSameNameException, IOException;

}

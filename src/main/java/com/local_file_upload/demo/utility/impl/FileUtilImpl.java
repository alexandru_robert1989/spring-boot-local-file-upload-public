package com.local_file_upload.demo.utility.impl;


import com.local_file_upload.demo.exception.DifferentFileWithSameNameException;
import com.local_file_upload.demo.utility.FileUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;

@Component
public class FileUtilImpl implements FileUtil {


    @Override
    public boolean fileNameExists(Path path, String fileName) {
        return Files.exists(path.resolve(fileName));
    }


    @Override
    public File getFileByName(Path path, String fileName) {
        return new File(path.resolve(fileName).toString());
    }


    @Override
    public String getFileExtension(MultipartFile file) {
        return FilenameUtils.getExtension(file.getOriginalFilename());
    }


    @Override
    public String renameFileBeforeRecording(Path root, String fileName) {

        if (Files.exists(root.resolve(fileName))) {
            int i = 1;
            while (true) {
                String newFileName = fileName + "(" + i + ")";
                Path tempPath = root.resolve(newFileName);
                if (!Files.exists(tempPath)) {
                    return newFileName;
                }
                i++;
            }
        }
        return fileName;
    }

    @Override
    public boolean isSameFile(File fileOne, File fileTwo) throws IOException {
        return FileUtils.contentEquals(fileOne, fileTwo);
    }

    @Override
    public File storeTemporaryFile(Path path, MultipartFile file, String temporaryFileName) throws IOException {
        Files.copy(file.getInputStream(), path.resolve(temporaryFileName));
        return new File(path.resolve(temporaryFileName).toString());
    }

    @Override
    public void deleteTemporaryFile(Path path, String fileName) throws IOException {
        Files.delete(path.resolve(fileName));
    }

    @Override
    public void handleSameNameExists(Path path, MultipartFile file) throws
            FileAlreadyExistsException, DifferentFileWithSameNameException, IOException {

        File storedFile = getFileByName(path, file.getOriginalFilename());
        //create temporary file name
        String temporaryFileName = renameFileBeforeRecording(path, file.getOriginalFilename());

        // store temporary file can throw IOException
        File temporaryFile = storeTemporaryFile(path, file, temporaryFileName);

        if (isSameFile(storedFile, temporaryFile)) {
            System.out.println("The file is already stored :" + storedFile.getName());

            // deleteOne throws IOException
            deleteTemporaryFile(path, temporaryFileName);
            System.out.println("The temporary file was deleted");

            throw new FileAlreadyExistsException("The file " + storedFile.getName() + " already exists");
        } else {
            System.out.println("Different file with same name exists");
            throw new DifferentFileWithSameNameException("A different file with the name '" + file.getOriginalFilename() + "' exists, the file was saved as '" +
                    temporaryFile.getName() + "'");
        }

    }

}

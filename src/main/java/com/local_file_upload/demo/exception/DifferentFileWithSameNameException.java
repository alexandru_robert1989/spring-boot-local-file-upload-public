package com.local_file_upload.demo.exception;

public class DifferentFileWithSameNameException extends RuntimeException {
    public DifferentFileWithSameNameException(String message) {
        super(message);
    }
}
